import XCTest
@testable import PickerComponentSwiftUI

final class PickerComponentSwiftUITests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(PickerComponentSwiftUI().text, "Hello, World!")
    }
}
