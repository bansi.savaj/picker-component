//
//  PickerView.swift
//  Picker Component
//
//  Created by bansi savaj on 04/10/23.
//

import SwiftUI

@available(iOS 13.0, *)
public struct MenuPickerView: View {
    
    @Binding var selectedColor: String
    var colors: [String]
    var tapOn: ((String) -> Void)?
    var onAppear: (() -> Void)?
    var foregroundColor: Color = .black
    var backgroundColor: Color = .clear
    var underlineColor: Color = .clear
    var accentColor: Color = .gray
    var font: Font = .headline
    
    public init(selectedColor: Binding<String>, colors: [String], tapOn: ( (String) -> Void)? = nil, onAppear: (() -> Void)? = nil, foregroundColor: Color = .black, backgroundColor: Color = .clear, underlineColor: Color = .clear, accentColor: Color = .black, font: Font = .callout) {
        self._selectedColor = selectedColor
        self.colors = colors
        self.tapOn = tapOn
        self.onAppear = onAppear
        self.foregroundColor = foregroundColor
        self.backgroundColor = backgroundColor
        self.underlineColor = underlineColor
        self.accentColor = accentColor
        self.font = font
    }
    
    public var body: some View {
        if #available(iOS 16.0, *) {
            Picker("Please choose a color", selection: $selectedColor) {
                ForEach(colors, id: \.self) {
                    Text($0)
                }
            }
            //        .pickerStyle(.menu)
            .onChange(of: selectedColor) { newValue in
                tapOn?(newValue)
            }
            .onAppear {
                onAppear?()
            }
            .foregroundColor(foregroundColor)
            //        .labelsHidden()
            .padding(2)
            .background(backgroundColor)
            .font(font)
            .underline(color: underlineColor)
            .accentColor(accentColor)
        } else {
            // Fallback on earlier versions
        }


    }
}

@available(iOS 13.0, *)
public struct WheelPickerView: View {
    
    @Binding var selectedPlanet: Planet
    var wheel: ((String) -> Void)?
    var onAppear: (() -> Void)?
    var foregroundColor: Color = .red
    var backgroundColor: Color = .clear
    var underlineColor: Color = .clear
    var accentColor: Color = .gray
    var font: Font = .headline
    
    public init(selectedPlanet: Binding<Planet>, wheel: ( (String) -> Void)? = nil, onAppear: ( () -> Void)? = nil, foregroundColor: Color = .black, backgroundColor: Color = .clear, underlineColor: Color = .clear, accentColor: Color = .black, font: Font = .callout) {
        self._selectedPlanet = selectedPlanet
        self.wheel = wheel
        self.onAppear = onAppear
        self.foregroundColor = foregroundColor
        self.backgroundColor = backgroundColor
        self.underlineColor = underlineColor
        self.accentColor = accentColor
        self.font = font
    }
    
    public var body: some View {
        if #available(iOS 16.0, *) {
            Picker("Planet", selection: $selectedPlanet) {
                ForEach(Planet.allCases) { planet in
                    Text(planet.rawValue.capitalized)
                        .foregroundColor(foregroundColor)
                    
                }
            }
            .pickerStyle(.wheel)
            .onChange(of: selectedPlanet) { newValue in
                wheel?(newValue.rawValue)
            }
            .onAppear {
                onAppear?()
            }
            //        .labelsHidden()
            .padding(2)
            .background(backgroundColor)
            .font(font)
            .underline(color: underlineColor)
            .accentColor(accentColor)
        } else {
            // Fallback on earlier versions
        }
    }

}


@available(iOS 13.0, *)
public struct SegmentPickerView: View {
    
    @Binding var selectedColor: String
    var segment: ((String) -> Void)?
    var onAppear: (() -> Void)?
    var foregroundColor: Color = .red
    var backgroundColor: Color = .yellow
    var underlineColor: Color = .clear
    var accentColor: Color = .gray
    var font: Font = .headline
    var cornerRadius: CGFloat = 10
    var colors: [String]
    
    public init(selectedColor: Binding<String>, segment: ( (String) -> Void)? = nil, onAppear: ( () -> Void)? = nil, foregroundColor: Color = .black, backgroundColor: Color = .clear, underlineColor: Color = .clear, accentColor: Color = .black, font: Font = .callout, cornerRadius: CGFloat = 10, colors: [String]) {
        self._selectedColor = selectedColor
        self.segment = segment
        self.onAppear = onAppear
        self.foregroundColor = foregroundColor
        self.backgroundColor = backgroundColor
        self.underlineColor = underlineColor
        self.accentColor = accentColor
        self.font = font
        self.cornerRadius = cornerRadius
        self.colors = colors
    }
    
    public var body: some View {
        if #available(iOS 16.0, *) {
            Picker("Please choose a color", selection: $selectedColor) {
                ForEach(colors, id: \.self) {
                    Text($0)
                        .foregroundColor(foregroundColor)
                }
            }
            .pickerStyle(.segmented)
            .onChange(of: selectedColor) { newValue in
                segment?(newValue)
            }
            .onAppear {
                onAppear?()
            }
            .foregroundColor(foregroundColor)
            .padding(10)
            .background(backgroundColor)
            .font(font)
            .underline(color: underlineColor)
            .cornerRadius(cornerRadius)
        } else {
            // Fallback on earlier versions
        }
    }
}


@available(iOS 13.0, *)
public struct NavigationPickerView: View {
    
    @Binding var selectedPlanet: Planet
    var navigation: ((String) -> Void)?
    var foregroundItemColor: Color = .black
    var foregroundColor: Color = .green
    var onAppear: (() -> Void)?
    var backgroundColor: Color = .clear
    var underlineColor: Color = .clear
    var accentColor: Color = .gray
    var font: Font = .callout
    
    public init(selectedPlanet: Binding<Planet>, navigation: ((String) -> Void)? = nil, foregroundItemColor: Color = .black, foregroundColor: Color = .black
         , onAppear: ( () -> Void)? = nil, backgroundColor: Color = .clear, underlineColor: Color = .black, accentColor: Color = .gray, font: Font = .callout) {
        self._selectedPlanet = selectedPlanet
        self.navigation = navigation
        self.foregroundItemColor = foregroundItemColor
        self.foregroundColor = foregroundColor
        self.onAppear = onAppear
        self.backgroundColor = backgroundColor
        self.underlineColor = underlineColor
        self.accentColor = accentColor
        self.font = font
    }

    public var body: some View {
        if #available(iOS 16.0, *) {
            Picker("Planet", selection: $selectedPlanet) {
                ForEach(Planet.allCases, id: \.self) { planet in
                    Text(planet.rawValue.capitalized)
                        .onTapGesture {
                            selectedPlanet = planet
                        }
                        .foregroundColor(foregroundItemColor)
                    
                }
            }
            .pickerStyle(.navigationLink)
            .onChange(of: selectedPlanet) { newValue in
                navigation?(newValue.rawValue)
            }
            .onAppear {
                onAppear?()
            }
            .foregroundColor(foregroundColor)
            //        .labelsHidden()
            .padding(2)
            .background(backgroundColor)
            .font(font)
            .underline(color: underlineColor)
            .accentColor(accentColor)
        } else {
            // Fallback on earlier versions
        }

    }
}



public enum Planet: String, CaseIterable, Identifiable {
    case mercury
    case venus
    case earth
    case mars
    case jupiter
    case saturn
    case uranus
    case neptune
    
    public var id: String { self.rawValue }
}

public var colors = ["Red", "Green", "Blue", "Tartan"]
